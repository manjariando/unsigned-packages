#!/bin/bash

cd pkgbuilds

buildpkg -b stable -c -p pacman-manjariando

cd pacman-manjariando

rm -rf pacman-contrib *.patch *.log *.xz

cd ..

buildarmpkg -b stable -p pacman-manjariando

cd pacman-manjariando

rm -rf pacman-contrib *.patch *.log *.xz

rm -rf  pkg src *.pkg.tar.zst

cd .. && cd ..
