#!/bin/bash

cd pacman-manjariando/x86_64

rm -rf pacman-manjariando-x86_64.pkg.tar.zst

cp -r pacman-manjariando-*-x86_64.pkg.tar.zst pacman-manjariando-x86_64.pkg.tar.zst

cd ..

cd aarch64

rm -rf pacman-manjariando-aarch64.pkg.tar.zst

cp -r pacman-manjariando-*-aarch64.pkg.tar.zst pacman-manjariando-aarch64.pkg.tar.zst

cd .. && cd ..

git add .

git commit -m "Update pacman-manjariando"

git push
